## Folder Structure

```
/
  src/
    html/
    css/
      question1/
    js/
      q1-for-loop-scope/
      q2-closure-counter/
      q3-use-strict/
      q4-hoisting/
      q5-static-methods/
      q6-delete/
      q7-destructuring/
      q8-arrow-functions/
      q9-spread-rest/
      q10-promises/
      q11-array-iteration/
      q12-classes/
      q13-const/
```

## Testing Concepts

### JS

* [x] scope - `js/q1-for-loop-scope`
* [x] closure - `js/q2-closure-counter`
  * [x] object method short form - `js/q2-closure-counter`
  * [x] getter - `js/q2-closure-counter`
* [x] use strict - `js/q3-use-strict/`
* [x] hoisting - `js/q4-hoisting`
* [x] delete operator - `js/q6-delete`
* [x] prototypes - `js/q5-static-methods`
* ES6/7
  * [x] Destructuring - `js/q7-destructuring`
  * [x] Arrow functions - `js/q8-arrow-functions`
  * [x] Spread/Rest - `js/q9-spread-rest`
  * [x] Promises - `js/q10-promises`
  * [x] Array Iteration - `js/q11-array-iteration`
  * [x] Class Syntax - `js/q12-classes`
  * [x] const - `js/q13-const`
* [x] Lint - (Code satisfies linter)

### HTML

* [ ] jQuery/Native selects
