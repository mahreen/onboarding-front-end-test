/* eslint-disable no-unused-vars,no-undef */

/**
 * # Question 7 - Destructuring
 */

const jane = {
  first: 'Jane',
  last: 'Doe',
  age: 23,
};

// 7a Requirement: Using destructuring, extract and rename jane.first to firstName.
// Start 7a code

// End 7a code

// --------------------------------------------------

// 7b Requirement: Using destructuring, extract the first item in the array into firstItem.
const nums = [1, 2, 3, 4];

// Start 7b code

// End 7b code

// Do not modify anything below this point
console.assert(firstName === 'Jane');
console.assert(firstItem === 1);
