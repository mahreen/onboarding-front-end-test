/* eslint-disable */

/**
 * # Question 8 - Arrow Functions
 *
 * 1. Why does this example not work
 * 2. Why would a regular function fix it
 */

const person = {
  first: 'Jane',
  last: 'Doe',
  fullName: () => this.first + ' ' + this.last,
};

console.log(person.fullName());

/*
  WRITE YOUR ANSWER HERE
  ----------------------

  ## Answer 1

  ## Answer 2

*/
