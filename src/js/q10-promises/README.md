# Question 10 - Promises

Create a function `delay(ms)` which is a promisified version of `setTimeout` and resolves after `ms` milliseconds

eg:
```javascript
delay(1000)
  .then(() => {
    console.log('I was logged 1000 ms after the call');
  })
```
